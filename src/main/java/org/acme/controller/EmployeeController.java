package org.acme.controller;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.acme.model.Employee;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;


@Path("/employee")
public class EmployeeController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        List<Employee> firstRange = Employee.list("Select e from Employee e");
        return Response.ok(firstRange).build();
    }
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list(@PathParam("id") Integer id) {
        List<Employee> firstRange = Employee.list("Select e from Employee e where id = ?1 " +
                "or id in (select id from Employee where manager_id =?1) " +
                "or manager_id in (select id from Employee where manager_id =?1)",id);
        return Response.ok(firstRange).build();
    }
}
