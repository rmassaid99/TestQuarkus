package org.acme.controller;


import org.acme.model.Factorial;


import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

@Path("/factorial")
public class FactorialController {

    @GET
    public Response listAllFactorial() {
        List<Factorial> List = Factorial.list("select f from Factorial f order by n ASC");
        return Response.ok().entity(List).build();
    }

    @Path("/{n}")
    @GET
    public Response listDetailFactorial(@PathParam("n") int n) {
        List<Factorial> List = Factorial.list("select f from Factorial f where n = ?1 order by n ASC ",n);
        return Response.ok().entity(List).build();
    }

    @POST
    @Path("/{n}")
    @Transactional
    public Factorial addFruit(@PathParam("n") int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        Factorial factorial = new Factorial();
        factorial.n = n;
        factorial.nfactorial = result;
        factorial.persist();
        return factorial;
    }

    @POST
    @Path("new/{n}")
    @Transactional
    public HashMap<Integer, Integer> addFactor(@PathParam("n") int n) {
        HashMap<Integer,Integer> factorial = new HashMap<Integer, Integer>();
        factorial.put(0,1);
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
            factorial.put(i,result);
        }

        return factorial;
    }
}
