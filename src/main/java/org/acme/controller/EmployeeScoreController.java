package org.acme.controller;

import org.acme.model.Employee;
import org.acme.model.EmployeeScore;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Path("/employeescore")
public class EmployeeScoreController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        List<EmployeeScore> firstRange = EmployeeScore.list("Select e from EmployeeScore e");
        return Response.ok(firstRange).build();
    }
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list(@PathParam("id") Integer id) {
        List<Employee> idEmployee = Employee.list("Select e.id from Employee e where id = ?1 " +
                "or id in (select id from Employee where manager_id =?1) " +
                "or manager_id in (select id from Employee where manager_id =?1)",id);

        List<EmployeeScore> score = new ArrayList<EmployeeScore>();
        for (int i = 0; i < idEmployee.size(); i++) {
            List<EmployeeScore> scores = EmployeeScore.list("Select es from EmployeeScore es where id = ?1", idEmployee.get(i));
            score.add(scores.get(0));
        }

        return Response.ok(score).build();
    }
    @Path("rate/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response Rate(@PathParam("id") Integer Id) {
        List<Employee> idEmployee = Employee.list("Select e.id from Employee e where id = ?1 " +
                "or id in (select id from Employee where manager_id =?1) " +
                "or manager_id in (select id from Employee where manager_id =?1)",Id);

        List<Integer> score = new ArrayList<Integer>();
        for (int i = 0; i < idEmployee.size(); i++) {
            List<EmployeeScore> scores = EmployeeScore.list("Select es from EmployeeScore es where id = ?1", idEmployee.get(i));
            score.add(scores.get(0).Score);
        }
        int total=0;
        for (int i = 0; i<score.size();i++){
            total += score.get(i);
        }
        Integer rate = total/score.size();
        HashMap<String,Integer> hasil = new HashMap<String, Integer>();
        hasil.put("Rata-rata",rate);
        return Response.ok(hasil).build();
    }
}
