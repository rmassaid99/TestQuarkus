package org.acme.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class EmployeeScore  extends PanacheEntityBase {
    @Id
    @Column(name = "id", nullable = false)
    public Integer id;

    @Column(length = 40)
    public String name;

    public Integer Score;
}
