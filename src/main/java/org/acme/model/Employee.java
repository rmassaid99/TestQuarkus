package org.acme.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
public class Employee extends PanacheEntityBase {
    @Id
    @Column(name = "id", nullable = false)
    public Integer id;

    @Column(length = 40)
    public String name;

    public Integer manager_id;

}
